import requests,os
import json
import base64
import time
from configparser import ConfigParser
from getpass import getpass
import collections.abc

import logging

logger = logging.getLogger("acarchive")

def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d

class ACarchive():
    """
    class which handles the communication with the ACarchive
    """
    def __init__(self,_url=None,_username=None,_password=None,_project='default',_configfile='AC.cfg',reinit=False):
        """
        initialize communication with ACarchive
    
        Parameters
        ----------
        url : string, optional
             url where AC archive is hosted. The default is defaulturl.
        username : TYPE, optional
             valid username of ACarchive The default is superusername.
        password : TYPE, optional
             valid password of ACarchive The default is superpassword.
        project : TYPE, optional
            project to which the class should connect, e.g.  default or catlab 
            The default is defaultproject.

        Returns
        -------
        None.

        """
        self.url = _url
        self.username = _username
        self.password = _password
        self.project = _project
        self.token = None
        self.valid_token = False
        if not reinit: 
            parser = ConfigParser()
            found = parser.read(_configfile)    
            if len(found) > 0:
                if 'default' not in parser:
                    raise ValueError("No credentials/url provided to connect to ACarchive")
                self.url = parser['default']['url'] if 'url' in parser['default'] else _url
                self.username = parser['default']['username'] if 'username' in parser['default'] else _username
                self.password = parser['default']['password'] if 'password' in parser['default'] else _password
                self.project = parser['default']['project'] if 'project' in parser['default'] else _project

            if  self.username == None:
                self.username = str(input("Username: "))
            if  self.password == None:
                self.password = str(getpass("Password: "))
            if  self.url == None:
                self.url = str(input("ACarchive url: "))

            self.getToken()
        
    @classmethod
    def reinit(cls,_url,_token,_project):
        """
        restarts a ACarchive session from a given token without the need of credentials

        Parameters
        ----------
        _url : string
            url of the server.
        _token : string
            valid token.
        _project : string
            name of the associated project.

        Returns
        -------
        ACarchive class object.

        """
        session =  cls(_url=_url,_project=_project,reinit=True)
        session.token = _token
        if session.getFields('data'): #test token
            session.valid_token = True
        return session
        
        
        
    def makeRequest(self,_endpoint,_payload):
        """
        makes a request to the ACarchive

        Parameters
        ----------
        endpoint : String
            endpointto be used.
        payload : dictionary
            dictionary to be send.

        Returns
        -------
        response_data : dictionary
            jsondictionary with the response

        """
        assert isinstance(_payload, dict)

        code = 403
        counter = 0
        while counter <= 5:
            counter+=1
            try:
                #   proxies = {"http": "http://www.bessy.de:3128", "https": "http://www.bessy.de:3128"}
                #response = requests.get(self.url+_endpoint,data=json.dumps(_payload), proxies=proxies)
                response = requests.get(self.url+_endpoint,data=json.dumps(_payload))
                response_data = json.loads(response.content.decode('utf8'))

                code = response.status_code
                if response_data['success'] == 0 and 'error' in response_data:  
                    logger.error(f"AC ARCHIVE: Error meassage: {response_data['error']}") 
                    if response_data['error'] == 'invalid token' and 'token' in _payload :
                       self.getToken() 
                       _payload['token'] = self.token
                    else:
                        return response_data
                            
                elif code != 200:
                    logger.error(f"AC ARCHIVE: error code= {code}, retrying")
                    response.raise_for_status()
    
                elif code == 200 and response_data['success'] == 1:
                    return response_data
                    
            except Exception as e:
                 logger.error("AC ARCHIVE: Error message ACarchive makeRequest: " ,e)
            time.sleep(0.5)
        return None
            
        
    def getToken(self):
        
        """
        use a username and password to get a token for the AC Archive using the 
        the specified url fromabove
    
        Parameters
        ----------
       
        Returns
        ------------
         token : string, string
         
             token is a string used to authenticate an ACarchive request session
                     
        """

        login_credentials = {"user":self.username,"password":self.password}
        #logger.info(login_credentials)
        response_data = self.makeRequest('/v1/login',login_credentials)

        if response_data and "token" in response_data: 
            token = response_data['token']
            if token:
                logger.info(f"AC ARCHIVE: Token is {token}")
                self.token = token
                self.valid_token = True
                return token
            else:
                logger.error("AC ARCHIVE: Error requesting Token. Response is None.")
    
    def getFields(self,_entrytype):
        """
        get fields for givenentry type
        
        Parameters
        ----------
        _entrytype : string
            entrytype, either data or sample
     
        Returns
        ------------
         fields : list of strings
             
        """
        assert _entrytype in  ['data','sample'] 
        payload = {"token":self.token,"type":_entrytype}
        response_data = self.makeRequest('/v1/fields',payload)
        if response_data: 
            if response_data['success'] == 1:
                fields = [response_data['fields'][field]['name'] for field in response_data['fields'] ]
                return fields
        return None
    
    def addLinks(self,_entryid,_links):
        """
        get fields for givenentry type
        
        Parameters
        ----------
        _entryid : string
             id for a sample or data
        _links : list of strings
            list of ids to be linked to _entryid
     
        Returns
        ------------
             
        """
        assert isinstance(_links, list)
        payload = {"token":self.token,"links":_links}
        response_data = self.makeRequest('/v1/addlinks/'+_entryid,payload)
        if response_data: 
            if response_data['success'] == 1:
                return 1
        return None

    def uploadBase64(self,_entryID,_filepath,_filename=None):
        """
        convert a given file in file path to base64 andupload it to AC archive
        to sample withthe given ID
    
        Parameters
        ----------
        entryID : string
            the entry id like S35296
        filepath : string
            the path to the file like 2022_02_08/nx/00002_8161523.hdf
        filename: string
            the name of the file in the AC archive new_name.hdf
            
        Returns
        ------------
         success : integer
         
             0 means no success, 1 means success             
        """
        if not _filename:
            _filename = os.path.basename(_filepath)
        
        with open(_filepath,'rb') as file:
            data = base64.b64encode(file.read()).decode('utf8')
        
        payload = {"token":self.token,"filename":_filename,"content":data}
        
        response_data = self.makeRequest('/v1/directupload/'+_entryID,payload)
        if response_data: 
            if response_data['success'] == 1:
                logger.info(f"AC ARCHIVE: Uploaded file {_filepath}")
                return response_data['docid']
        return None 
        
    def downloadFile(self,_entryID,_docID,_filename):
        """
        Downloads a file
        
        Parameters
        ----------
        _entryID : String
            entry id eg. S456 or D545.
        _docid : String
            document id 1234.
        _filename : String
            nameofoutput file

        Returns
        -------
        None.

        """
        _docID = str(_docID)
        payload = {"token":self.token}
        response = requests.get(self.url +'/v1/download/'+_entryID+'/'+_docID,data=json.dumps(payload))
        if response: 
            with open(_filename, "wb") as f:
                f.write(response.content)
                logger.info(f"AC ARCHIVE: Downloaded file {_filename}")
                return 1
        else:
            logger.error("AC ARCHIVE: Failed Downloading file")
        return None
            


    def createEntry(self,_data,_entrytype='sample',_parent=None):
        """
        creates a new sample in the AC archive
    
        Parameters
        ----------
        data : json
            dictionary containing the data for the AC archive
            possbile fields are: "Title","Author","Comment","Abstract","Keywords","Document Type","Methods","Elements"
        entrytype string
            either 'sample' or 'data'
            
        Returns
        ------------
         sample Id : string
         
             the Sample ID creatd by the AC archive, like  S35296          
        """
        assert isinstance(_data, dict)
        assert _entrytype in  ['data','sample'] 
        
        payload = {"token":self.token,"type":_entrytype,"project":self.project}
        if _parent:
            payload.update({"parent":_parent})
        fields = self.getFields(_entrytype)
        for i,field in enumerate(fields):
            if field in _data:
                payload['f'+str(i)] = _data[field]
        
        response_data = self.makeRequest('/v1/new',payload)
        if response_data: 
            if  response_data['success'] == 1:
                entryID = response_data['id']
                logger.info(f"AC ARCHIVE: Created Entry, entryID: {entryID}")
                return entryID  
        return None
    
    def editEntry(self,_entryID,_data):
        """
        edit a given sample in the AC archive
    
        Parameters
        ----------
        _entryID string
            id of entry to bechanged,e.g. S456
        data : json
            dictionary containing the data for the AC archive
            possbile fields are: "Title","Author","Comment","Abstract","Keywords","Document Type","Methods","Elements"

        Returns
        ------------
         
        """
        assert isinstance(_data, dict)
        entrytype = None
        if _entryID[0] == 'S':
            entrytype = 'sample'
        elif _entryID[0] == 'D':
            entrytype = 'data'
        assert entrytype
        payload = {"token":self.token}
        fields = self.getFields(entrytype)
        for i,field in enumerate(fields):
            if field in _data:
                payload['f'+str(i)] = _data[field]
        
        response_data = self.makeRequest('/v1/edit/'+_entryID,payload)
        if response_data: 
            if  response_data['success'] == 1:
                entryID = response_data['id']
                logger.info(f"AC ARCHIVE: Edited Entry, entryID: {entryID}")
                return 1
            else:    
                logger.error(f"AC ARCHIVE: Failed editing Entry, entryID: {_entryID}")
        return None

    
    def deleteEntry(self,_entryID):
        """

        Parameters
        ----------
        _entryID : string
            ID of entry which should be deleted, e.g. S5432.

        Returns
        -------
        None.

        """
        payload = {"token":self.token}
        response_data = self.makeRequest('/v1/delete/'+_entryID,payload)
        if response_data: 
            if  response_data['success'] == 1:
                logger.info(f"AC ARCHIVE: Deleted entry {response_data['id']}, wanted to delete {_entryID}")
                return 1
            else:
                logger.error(f"AC ARCHIVE: Did not delete {_entryID}")
        return  None
        
    def getEntry(self,_entryID):
        """
        get data of entry forgiven entry ID
        Parameters
        ----------
        _entryID : string
            ID of entry which should be deleted, e.g. S5432.

        Returns
        -------
        dat.

        """
        payload = {"token":self.token}
        response_data = self.makeRequest('/v1/get/'+_entryID,payload)
        if response_data: 
            if  response_data['success'] == 1:
                logger.info(f"AC ARCHIVE: got data for entry {response_data['id']}, wanted data for entry {_entryID}")
                return response_data
            else:
                logger.error(f"AC ARCHIVE: did not get data for entry {_entryID}")
        return None
        
    def search(self,_values):
        """
        search for the given values in all fields

        Parameters
        ----------
        _values : list strings
            list of values to be searched for .
        
        Returns
        -------
        None.

        """
        payload = {"token":self.token,"search":[{"keys":_values,"fields":[i for i in range(32)]}]}
        response_data = self.makeRequest('/v1/search',payload)
        if response_data: 
            if  response_data['success'] == 1:
                logger.info(f"AC ARCHIVE: success search was completed")
                return response_data["result"]
            else:
                logger.error(f"AC ARCHIVE: search request failed")
        return None
    
    def jsonget(self,_entryID):
        """
        get the json data for a given ID

        Parameters
        ----------
        _entryID: string
            ID of the entry type, e.g. S123, D34
        
        Returns
        -------
        json attached to the entry.

        """
        payload = {"token":self.token}
        response_data = self.makeRequest('/v1/jsonget/'+_entryID,payload)
        if response_data: 
            if  response_data['success'] == 1:
                logger.info(f"AC ARCHIVE: success jsonget was completed, got for ID {_entryID} \n{ response_data['jasondata']}")
                if response_data["jasondata"]:
                    return response_data["jasondata"]
                else:
                    return {}
            else:
                logger.error(f"AC ARCHIVE: jsonget for ID {_entryID} request failed")
        return None
    
    def jsonsearch(self,_key,_mode='match',_value=None):
        """
        search for all entries with given key

        Parameters
        ----------
        _key : string
            key to be searched for
        
        Returns
        -------
        Dictionary of results.

        """
        
        assert _mode in  ['match','range'] 
        
        
        payload = {"token":self.token,"jsonsearch":_key}
        if _value:
            if _mode == 'range':
                assert isinstance(_value,list) and len(_value) == 2
            payload.update({_mode:_value})
            
        response_data = self.makeRequest('/v1/jsonsearch',payload)
        if response_data: 
            if  response_data['success'] == 1:
                logger.info(f"AC ARCHIVE: success jsonsearch was completed, found \n {response_data['result'] }")
                if response_data['result']:
                    return response_data['result']
                else: 
                    return {}
            else:
                logger.error(f"AC ARCHIVE: jsonsearch request failed")
        return None
    
    def jsonadd(self,_values,_entryID):
        """
        add json to given entry, overwrites old json!!!

        Parameters
        ----------
        _values : dict
            python dictionary with the key value pairs
        _entryID: string
            ID of the entry type, e.g. S123, D34
        
        Returns
        -------
        None.

        """
        assert isinstance(_values,dict)
        payload = {"token":self.token,"jsondata":_values}
        response_data = self.makeRequest('/v1/jsonadd/'+_entryID,payload)
        if response_data: 
            if  response_data['success'] == 1:
                logger.info(f"AC ARCHIVE: success jsonadd was completed, json is now {_values}")
                return 1
            else:
                logger.error(f"AC ARCHIVE: jsonadd request failed")
        return None
    
    
    def jsonupdate(self,_values,_entryID):
        """
        updates json to given entry, by first fetching and then updating

        Parameters
        ----------
        _values : dict
            python dictionary with the key value pairs
        _entryID: string
            ID of the entry type, e.g. S123, D34
        
        Returns
        -------
        None.

        """
        assert isinstance(_values,dict)
        jsondata = self.jsonget(_entryID)
        if jsondata != None:
            assert isinstance(jsondata,dict)
            update(jsondata,_values)
            success = self.jsonadd(jsondata,_entryID)
            return 1 if success == 1 else None
        return None
    
    # def fixEntry(self,_entryID):
    #     """
    #     fixes the given entry, the entry can then not be altered anymore

    #     Parameters
    #     ----------
    #     _entryID : string
    #         id of the entry which shall be fixed.

    #     Returns
    #     -------
    #     None.

    #     """
    #     payload = {"token":self.token}
    #     response_data = self.makeRequest('/v1/fix/'+_entryID,payload)
    #     if response_data: 
    #         if  response_data['success'] == 1:
    #             print(f"AC ARCHIVE: fixed entry with id {_entryID}")
    #             return 1
    #         else:
    #             print(f"AC ARCHIVE: fixing entry failed")
    #     return None
