import pytest
import os,sys
## Set up env


from ACarchive.ACarchive import ACarchive



### implement the tests
    
def test_connecting_to_archive():
    session = ACarchive()
    assert len(session.token) > 2
    session.token = '1'
    assert isinstance(session.getFields("sample"),list)
    assert len(session.token) > 2

    
session = ACarchive()

def test_creating_and_deleting_entry():
    entryID = session.createEntry({"Preparator":"Micha"},_entrytype='sample')
    assert entryID[0] == "S"
    assert session.editEntry(entryID,{'Source':'Bessy'}) == 1
    data = session.getEntry(entryID)
    assert data["Preparator"] == "Micha" and data["Source"] == "Bessy"
    assert session.deleteEntry(entryID) == 1

def test_adding_links():
    entryID1 = session.createEntry({"Preparator":"Micha"},_entrytype='sample')
    entryID2 = session.createEntry({"Author":"Micha"},_entrytype='data')
    assert session.addLinks(entryID1,[entryID2]) == 1
    response = session.getEntry(entryID1)    
    assert response["links"][0]["link"] == entryID2
    assert session.deleteEntry(entryID1) == 1
    assert session.deleteEntry(entryID2) == 1


    

        
    
