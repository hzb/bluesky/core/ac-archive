import sys
from os import path

from setuptools import find_packages, setup

import versioneer

setup(name='ACarchive',
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass(),
      description='A python library to access the AC Archive API of FHI',
      url='https://gitlab.helmholtz-berlin.de/sissy/experiment-control/emiltools',
      author='Michael Goette',
      author_email='michael.goette@helmholtz-berlin.de',
      # license='MIT',
      packages=find_packages(exclude=['docs', 'tests']),
      install_requires=[
      ]
      # zip_safe=False
)
